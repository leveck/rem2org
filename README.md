# rem2org

The script I use to convert my remind calendar to org mode for use in orgzly on android.

NOTE: I keep my remind calendar simple on purpose. Lines much beyond:

```
REM 2020-01-01T00:00 +7 MSG Happy New Year
```

Likely will not be handled correctly.

Also, you'll need to change the file paths to your own.